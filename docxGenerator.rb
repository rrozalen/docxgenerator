require 'zip'
require './docxReader'

# Config path name
CONFIG_PATH = 'config/'

# Config file name
CONFIG_FILE = 'config.txt'

# Docx data file. Susceptible to change with word .docx estructure
DOC_FILE = 'word/document.xml'

# Replace current file if exists
Zip.on_exists_proc = true

# Use unicode names (for Windows <7)
Zip.unicode_names = true

# Reads docx file and generate a number of modified docs
def generate_docx(data)
  # -------- DOCX READING -------- #

  # Take .docx file_name
  docs = Dir['*.docx']
  if docs.size != 1
    puts 'Error. There is more (or less) than 1 docx file in the selected path'
    Kernel.exit
  end

  f = docs[0]
  file_name = f.split('.')
  file_name = file_name.take(file_name.size - 1).join('.')

  # Take doc data
  zip_file_data = {}
  doc_data = nil
  Zip::File.open(f) do |zip_file|
    zip_file.each do |e|
      zip_file_data[e.name] = e.get_input_stream.read if e.name != DOC_FILE
      doc_data = zip_file.read(DOC_FILE).force_encoding('utf-8')

      # Replace shared tags
      data[:shared].each do |rule|
        doc_data.gsub!(rule[:tag], rule[:text])
      end
    end
  end

  # -------- DOCX WRITING -------- #
  data[:exclusive].each do |ex|
    # Create folders
    path = file_name + ' - ' + ex[:name]
    Dir.mkdir path unless File.exist?(path)

    # Set exclusive rules
    doc_rules = doc_data
    ex[:rules].each do |rule|
      doc_rules = doc_rules.gsub(rule[:tag], rule[:text])
    end
    puts doc_rules
    # Generate 1 file for each exclusive element
    ex[:elements][:elements].each do |elem|
      doc = doc_rules.gsub(ex[:elements][:tag], elem)
      buffer = Zip::OutputStream.write_buffer do |out|
        data_array = zip_file_data.keys
        data_array.each do |name|
          out.put_next_entry(name)
          out.write zip_file_data[name]
        end
        out.put_next_entry(DOC_FILE)
        out.write doc
      end
      File.open(path + '/' + path + ' - ' + elem + '.docx', 'wb') { |out| out.write(buffer.string) }
    end
  end
end

generate_docx(readInputFiles(CONFIG_PATH, CONFIG_FILE))

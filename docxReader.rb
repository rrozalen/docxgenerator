# encoding: UTF-8
Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

# Configuration and input file tokens
SHARED_TK = 'shared'
EXCLUSIVE_TK = 'exclusive'
SH_TK = 'sh'
SEPARATION_TK = ' :: '

def read_shared_file(input_path, shared_file)
  shared_list = []
  File.open(input_path + shared_file, 'r').each_line do |l|
    array = l.delete("\n").split(SEPARATION_TK)
    shared = {}
    shared[:tag] = array.first
    shared[:text] = array.last
    shared_list << shared
  end
  shared_list
end

def read_exclusive_file(file)
  state = 0
  tags = elements = []
  File.open(input_path + file, 'r').each_line do |l|
    array = l.delete("\n").split(SEPARATION_TK)
    state += 1 if array.first != SH_TK
    if state == 0
      tag = {}
      tag[:tag] = array[1]
      tag[:text] = array.last
      tags << tag
    else
      elements << array.first
    end
  end
  elements
end

def read_exclusive_files(exclusive_files, exclusive_tag)
  exclusive_tags = []
  exclusive_files.each do |file|
    elements = read_exclusive_file(file)

    # Fill data structure
    exclusive_elements = {}
    elem = { tag: exclusive_tag, elements: elements }

    name = file.split('.')
    exclusive_elements[:name] = name.take(name.size - 1).join('.')
    exclusive_elements[:elements] = elem
    exclusive_elements[:rules] = tags
    exclusive_tags << exclusive_elements
  end
  exclusive_tags
end

# File reading method. Returns data estructure
def read_input_files(input_path, config_file)
  docs = Dir[input_path + config_file]
  return nil unless docs.size == 1

  shared_file = exclusive_tag = exclusive_files = nil
  data = {}

  # Read config file
  File.open(input_path + config_file, 'r').each_line do |l|
    l = l.force_encoding('utf-8')
    array = l.delete("\n").split(SEPARATION_TK)
    case array.first
    when SHARED_TK
      shared_file = array.last
    when EXCLUSIVE_TK
      exclusive_tag = array[1]
      exclusive_files = array.last.split(' ')
    end
  end

  data[:shared] = read_shared_file
  data[:exclusive] = read_exclusive_files(exclusive_files, exclusive_tag)

  puts data
  data
end

#DocxGenerator README #

Generates a number of .docx files using a pattern given

eg: 
You want to write customized docs to students of 3 different classes. 
Each class will have a different message and each student will have a different name.

===== TERMINOLOGY =====

- TAG: Text you want to change. Any tag is valid

eg: [STUDENT] / +++SUBJECT+++ / >DATE< / etc

- SHARED: Tags change is equal for all documents
     
```
[DATE] :: 12/12/2014
```

- EXCLUSIVE: Tags we want to differentiate between groups
 
```
   [SUBJECT] :: Mathematics / Philosophy
   [CLASS] :: 1ºA / 1ºB / 1ºC
```


- SPECIAL TAG: Receiver name. Will generate 1 doc for each name
```
     [STUDENT] :: Bob Smith / etc 
```

  
===== USAGE =====





You can use the docxGenerator script or use the Shoes GUI. 

NOTICE: An example of config files is given. 

----- docxGenerator -----

ruby docxGenerator.rb

A .docx pattern must be placed in the same path as this script. (The same for docxReader.rb)

You must give the config files (in the config/ path):

- shared file (config/shared.txt)

All the docx will modify the shared tags (like dates and so)

     TAG :: text
     TAG2 :: text2
     [...]

- exclusive files (config/name1.txt)
You can add as files as you want. Every file will modify the given tags in a different way. At the end you must add all the receiver names of this group.

```
     TAG :: text1
     TAG2 :: text2
     [...]
     Name1
     Name2
     [...]

```

- Config file (config/config.txt)
 
First line: shared file name

     shared :: shared.txt

Second line: exclusive file names and shared tag

     exclusive :: TAG :: Files separated by whitespaces 



An example of .docx pattern and config files is given


----- GUI -----
```
shoes docxGeneratorGUI.rb
```

The purpose of this GUI is to write the config files in a friendlier way. It is divided in two views:

- 1º View: Choose .docx pattern and ALL tags written in the pattern. 
- 2º Group edition: You can add as many groups as you can, but you must fill all the inputs. 

You can change the language by modifying the constant LNG(languages :EN, :ES)

The GUI will read any config file from input/ path and copy them with the GUI modifications to config/ in order to call the docxGenerator

NOTICE: The GUI can't call the script properly in UNIX. Write ruby docxGenerator.rb when the config files are generated to solve it.  


====== Data structure ======

Image given:

- [] --> List
- Str --> String
- { key => value } ---> Hash

![IMG_20141006_005448.jpg](https://bitbucket.org/repo/rozryA/images/1499466628-IMG_20141006_005448.jpg)
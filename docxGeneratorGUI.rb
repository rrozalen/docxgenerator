# encoding: UTF-8

require './docxReader'

# TEXT VARIABLES: Languages ES/EN
TEXTS = {}
TEXTS[:EN] = TEXTS[:ES] = {}

TEXTS[:EN][:changeIt] = 'Change it'
TEXTS[:EN][:invalidFile] = 'Invalid type of file'
TEXTS[:EN][:shared?] = 'Shared?'
TEXTS[:EN][:choosePattern] = 'Choose the pattern'
TEXTS[:EN][:selectDocx] = 'Select a .docx pattern'
TEXTS[:EN][:chooseSpecial] = 'Choose your special tag'
TEXTS[:EN][:chooseTags] = 'Choose your other tags'
TEXTS[:EN][:next] = 'Next'
TEXTS[:EN][:invalidPattern] = 'Choose a valid pattern'
TEXTS[:EN][:finish] = 'FINISH'
TEXTS[:EN][:newFile] = 'Please insert the name of the new file'
TEXTS[:EN][:rmFile] = 'Please insert the name of the file to remove'

TEXTS[:ES][:changeIt] = 'Cambiar'
TEXTS[:ES][:invalidFile] = 'Tipo de documento no válido'
TEXTS[:ES][:shared?] = 'Compartido?'
TEXTS[:ES][:choosePattern] = 'Escoge la plantilla'
TEXTS[:ES][:selectDocx] = 'Selecciona una plantilla de formato .docx'
TEXTS[:ES][:chooseSpecial] = 'Escoge la etiqueta especial'
TEXTS[:ES][:chooseTags] = 'Escoge las demás etiquetas'
TEXTS[:ES][:next] = 'Siguiente'
TEXTS[:ES][:invalidPattern] = 'Plantilla inválida'
TEXTS[:ES][:finish] = 'HECHO'
TEXTS[:ES][:newFile] = 'Introduce el nombre del nuevo fichero'
TEXTS[:ES][:rmFile] = 'Introduce el nombre del fichero a eliminar'

# Config path
CONFIG_PATH = 'config/'

# INPUT
INPUT_PATH = 'input/'

# Config file name
CONFIG_FILE = 'config.txt'

# Target file extension
EXT = '.docx'

# Shared file
SHARED_FILE = 'shared.txt'

# Language
LNG = :ES

# Configuration and input file tokens
SHARED_TK = 'shared'
EXCLUSIVE_TK = 'exclusive'
SH_TK = 'sh'
SEPARATION_TK = ' :: '

# Choose the text using the selected language
def language(str)
  TEXTS[LNG][str]
end

# Gets the file data of the given file_name
def get_data_file(data, file_name)
  data[:exclusive].each { |file| return file if file[:name] + '.txt' == file_name }
  nil
end

# Gets the matching text of the tag given
def get_tag_text(list, tag)
  list.each { |e| return e[:text] if e[:tag] == tag }
  nil
end

Shoes.show_log
Shoes.app do

  # Choosing pattern layout
  def add_pattern(file)
    name = para file
    button language(:changeIt) do
      path_name = ask_open_file
      file_name = path_name.split('/').last
      if !file_name.include? EXT
        name.text = language(:invalidFile)
      else
        docs = Dir['*' + EXT]
        docs.each { |doc| FileUtils.rm(doc) }
        FileUtils.cp(path_name, '/' + Dir.pwd + '/' + file_name)
        name.text = file_name
      end
    end
  end

  # Choosing tags layout
  def add_tag(name, shared)
    f = flow(margin_left: 12) do
      e = edit_line width: 200, margin_right: 10
      e.change { x = e.text }
      para language(:shared?)
      c = check
      button 'x', margin_left: 15 do
        f.clear.remove
      end
      e.text = name unless name.nil?
      c.checked = shared
    end
  end

  # Gets the layout of the file tagged as the name given
  def get_layout(layout, name)
    layout.contents().each do |l|
      l.contents.each do |e|
        return l if e.class == Shoes::Types::Tagline && e.text.force_encoding('utf-8') == name.force_encoding('utf-8')
      end
    end
    nil
  end

  # Adds a "Choose pattern" layout
  def add_file(file_name, tags_data, data)
    stack(margin_left: 12, margin_right: 12) do
      border black
      tagline(file_name, margin_left: 12)
      if file_name == SHARED_FILE
        tags_data[:shared].each do |tag|
          flow do
            para tag, margin_left: 12
            e = edit_line(width: 400, margin_left: 12).change { x = e.text }
            e.text = get_tag_text(data[:shared], tag) unless data.nil?
          end
        end
      else
        para tags_data[:special], margin_left: 12
        tags_data[:exclusive].each do |tag|
          flow do
            para tag, margin_left: 12
            e = edit_line(width: 400, margin_left: 12).change { x = e.text }
            unless data.nil?
              data_file = get_data_file(data, file_name)
              e.text = get_tag_text(data_file[:rules], tag) if !data_file.nil?
            end
          end
        end
        e = edit_box(margin_left: 12, width: 400)
        unless data.nil?
          data_file = get_data_file(data, file_name)
          e.text = data_file[:elements][:elements].join("\n") if !data_file.nil?
        end
      end
    end
  end

  # Write the files in a configuration path
  def write_files(views)
    FileUtils.rm_rf(CONFIG_PATH)
    Dir.mkdir CONFIG_PATH unless File.directory? CONFIG_PATH
    special_tag = nil
    files = ''
    views.contents.each do |view|
      file = file_name = line = nil
      view.contents.each do |elem|
        case elem
        when Shoes::Types::Tagline
          file_name = elem.text.force_encoding('utf-8')
          files += file_name + ' ' unless file_name == SHARED_FILE
          file = File.open(CONFIG_PATH + file_name, 'w:UTF-8')

        when Shoes::Types::Para
          special_tag = elem.text

        when Shoes::Types::Flow
          elem.contents.each do |e|
            case e
            when Shoes::Types::Para
              file_name == SHARED_FILE ? line = '' : line = SH_TK + SEPARATION_TK
              line += e.text
            when Shoes::Types::EditLine
              line += SEPARATION_TK + e.text + "\n"
            end
          end
          file.write(line.force_encoding('utf-8'))

        when Shoes::Types::EditBox
          file.write(elem.text.force_encoding('utf-8'))
        end
      end
      file.close
    end

    # Config file
    file = File.open(CONFIG_PATH + CONFIG_FILE, 'w:UTF-8')
    line = SHARED_TK + SEPARATION_TK + SHARED_FILE + "\n"
    line += EXCLUSIVE_TK + SEPARATION_TK + special_tag + SEPARATION_TK + files
    file.write(line.force_encoding('utf-8'))
    file.close
  end

  # VIEW 1. CHOOSE PATTERN AND TAGS
  def view_1(data)
    # Pattern section
    stack(margin: 12) do
      border black
      para language(:choosePattern), align: 'center'
      flow(margin: 12) do
        docs = Dir['*' + EXT]
        docs.size != 1 ? add_pattern(language(:selectDocx)) : add_pattern(docs[0])
      end
    end

    # Special tag section
    special = stack(margin: 12) do
      border black
      para language(:chooseSpecial), align: 'center'
      special_tag = edit_line(margin_left: 12).change { x = special_tag.text }
      special_tag.text = data[:exclusive].first[:elements][:tag] unless data.nil?
    end

    # Tags section
    tags = stack(margin: 12) do
      border black
      para language(:chooseTags), align: 'center'

      if data.nil?
        add_tag(nil, false)
      else
        data[:shared].each { |t| add_tag(t[:tag], true) }
        data[:exclusive].first[:rules].each { |t| add_tag(t[:tag], false) }
      end

      b = button '+', margin_left: 12 do
        tags.before(b) { add_tag(nil, false) }
      end
    end

    # Next button
    button language(:next), margin: 12, align: 'right' do
      docs = Dir['*' + EXT]
      if docs.size == 1
        tags_data = {}
        tags_data[:shared] = []
        tags_data[:exclusive] = []

        special.contents.each { |s| tags_data[:special] = s.text if s.class == Shoes::Types::EditLine }
        tags.contents.each do |flow|
          tag = nil
          if flow.class == Shoes::Types::Flow
            flow.contents.each do |elem|
              if elem.class == Shoes::Types::EditLine && !elem.text.empty?
                tag = elem.text
              elsif !tag.nil? && elem.class == Shoes::Types::Check
                elem.checked? ? tags_data[:shared] << tag : tags_data[:exclusive] << tag
              end
            end
          end
        end

        @view_1.hide
        view_2(tags_data, data)
      else
        alert language(:invalidPattern)
      end
    end
  end

  # VIEW 2. CHOOSE THE DIFFERENT DOCUMENTS AND FILL THE TAGS
  def view_2(tags, data)
    file_list = file_layout = nil
    current_file = SHARED_FILE
    stack(margin: 12) do
      flow(margin: 12) do
        files = []
        files << SHARED_FILE
        data[:exclusive].each { |f| files << (f[:name] + '.txt') } unless data.nil?

        file_list = list_box items: files
        file_list.change do
          get_layout(file_layout, current_file).hide
          current_file = file_list.text
          get_layout(file_layout, current_file).show
        end
        button '+' do
          name = ask(language(:newFile))
          file_list.items = file_list.items << name
          file_list.choose(name)
          get_layout(file_layout, current_file).hide
          file_layout.append { add_file(name, tags, data) }
          current_file = name
        end
        button '-' do
          name = ask(language(:rmFile))
          if current_file == name
            file_list.choose(SHARED_FILE)
            get_layout(file_layout, name).hide
            get_layout(file_layout, SHARED_FILE).show
            current_file = SHARED_FILE
          end
          # Shoes is too buggy to delete directly the item
          l = file_list.items.dup
          l.delete(name)
          file_list.items = l
          get_layout(file_layout, name).clear.remove
        end
      end
      file_layout = stack do
        file_list.items.each { |file| add_file(file, tags, data).hide }
      end
      get_layout(file_layout, SHARED_FILE).show
      button language(:finish), margin: 12, align: 'right' do
        write_files(file_layout)

        # NOTICE: It doesn't work with Shoes in UNIX.
        system('ruby', 'docxGenerator.rb')
      end
    end
  end

  # MAIN CODE

  # Load data from input files
  data = readInputFiles(INPUT_PATH, CONFIG_FILE)

  @view_1 = stack do
    view_1(data)
  end
end
